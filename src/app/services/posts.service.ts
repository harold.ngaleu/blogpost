import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Post } from '../models/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  posts: Post[] = 
  [
    {
      title: 'Angular',
      content: 'Angular est un cadriciel (framework) coté client open source basé sur TypeScript dirigée par l\'équipe du projet Angular à Google et par une communauté de particuliers et de sociétés. Angular est une réécriture complète de AngularJS, cadriciel construit par la même équipe.',
      loveIts : 2,
      created_at :  new Date()
    },
    {
      title: 'Java',
      content: 'Java est le nom d’une technologie mise au point par Sun Microsystems (racheté par Oracle en 2010) qui permet de produire des logiciels indépendants de toute architecture matérielle. Cette technologie s’appuie sur différents éléments qui, par abus de langage, sont souvent tous appelés Java ',
      loveIts : 0,
      created_at : new Date()
    },
    {
      title: 'TypeScript',
      content: 'TypeScript est un langage de programmation libre et open source développé par Microsoft qui a pour but d\'améliorer et de sécuriser la production de code JavaScript. C\'est un sur-ensemble de JavaScript (c\'est-à-dire que tout code JavaScript correct peut être utilisé avec TypeScript)',
      loveIts : -1,
      created_at : new Date()
    }
  ];

  postsSubject = new Subject<Post[]>();


 
  constructor() {
    this.emitPosts() ;
}


  emitPosts() {
    this.postsSubject.next(this.posts.slice());
  }

  savePosts(newPost:Post) {
    console.log('Enregistrement... post ' + newPost.title);
    this.posts.push(newPost);
    this.emitPosts();

}


Dontloveit(p:Post){
  let val = this.posts.find(
    (post) => {
      if(p === post) {
        post.loveIts--;
        this.emitPosts();
        console.log('post = ' + post.title + ' mis à jour  loveIts = '+post.loveIts);
        return true;
      }
    }
  )
  }
  
Loveit(p:Post) {
let val = this.posts.find(
  (post) => {
    if(p === post) {
      post.loveIts++;
      this.emitPosts();
      console.log('post = ' + post.title + ' mis à jour  loveIts = '+post.loveIts);
      return true;
    }
  }
)
}

removePostByTitre(p:Post) {
  const postIndexToRemove = this.posts.findIndex(
    (post) => {
      if(post === p) {
        return true;
      }
    }
  );

  this.posts.splice(postIndexToRemove, 1);
  this.emitPosts();

}



}
