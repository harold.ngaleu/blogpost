import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Post } from '../models/post.model';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post: Post;
 
  constructor(public postsService:PostsService) { 

 }


 onDeletePost(){ 
    this.postsService.removePostByTitre(this.post);
 }

 ngOnInit() {
 }

 onDontloveit(){
  this.postsService.Dontloveit(this.post);
  }
  
onLoveit(post:Post) {
  this.postsService.Loveit(this.post);
}

getColor() {
   if(this.post.loveIts >0) {
     return 'green';
   } else if(this.post.loveIts <0) {
     return 'red';
   }
}

isLike(){
 if(this.post.loveIts>0)
 {
   return true;
 } 
 else {
   return false; 
 }
}
}
