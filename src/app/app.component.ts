import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-post';
  posts =
    [
      {
        title: 'Mon Premier post',
        content: 'Avant de plonger dans les différents dossiers.',
        loveIts : 2,
        created_at :  new Date()
      },
      {
        title: 'Mon deuxieme post',
        content: 'Maintenant vous pouvez lancer le serveur de développement ',
        loveIts : 0,
        created_at : new Date()
      },
      {
        title: 'Encore un post',
        content: 'Une fois le projet compilé, ouvrez votre navigateur',
        loveIts : 3,
        created_at : new Date()
      }
    ];

}
